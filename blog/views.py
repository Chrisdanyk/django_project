from django.http import HttpResponse
from django.shortcuts import render

from blog.models import Post

posts = [
    {'author': 'chris dany',
     'title': 'Spring boot microservices - Part 1',
     'content': 'First Post Content',
     'date_posted': 'August 28, 2018'},
    {'author': 'chris dany',
     'title': 'Spring boot microservices - Part 2',
     'content': 'First Post Content',
     'date_posted': 'August 29, 2019'}
]


# Create your views here.
def home(request):
    context = {'posts': Post.objects.all()}
    return render(request, 'blog/home.html',context=context)


def about(request):
    context = {'title': 'About'}
    return render(request, 'blog/about.html',context=context)
